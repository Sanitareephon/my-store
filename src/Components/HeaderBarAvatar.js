import {
  GradeSharp,
  LoginOutlined,
  PermIdentityOutlined,
} from "@mui/icons-material";
import LogoutIcon from "@mui/icons-material/Logout";
import {
  Avatar,
  Divider,
  IconButton,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Popover,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { getUserIdLS, userSignOutLS } from "../Utils/utils";
import "./HeaderBarAvatar.css";
import LoginDialog from "./LoginDialog";

const HeaderBarAvatar = (props) => {
  const navigate = useNavigate();
  const [anchorEl, setAnchorEl] = useState(null);
  const [isUserLogIn, setIsUserLogIn] = useState(false);
  const [isOpenDialog, setIsOpenDialog] = useState(false);
  const isOpenMenuList = Boolean(anchorEl);

  const handlePopoverOpen = (event) => {
    const userId = getUserIdLS();
    setIsUserLogIn(!!userId);
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const handleOnItemButtonClick = (path) => {
    handlePopoverClose();
  };

  const setUserSingOut = () => {
    navigate("/");
    userSignOutLS();
  };

  const handleOnSignInClick = () => {
    setIsOpenDialog(true);
    handlePopoverClose();
  };
  const handleDialogClose = () => setIsOpenDialog(false);

  return (
    <div>
      <IconButton
        aria-describedby="mouse-over-popover"
        onClick={handlePopoverOpen}
      >
        <Avatar
          alt="Avatar"
          className="header-bar-avatar-icon"
          sx={{ width: 56, height: 56 }}
        />
      </IconButton>
      <Popover
        id="mouse-over-popover"
        open={isOpenMenuList}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
        onClose={handlePopoverClose}
        disableRestoreFocus
      >
        <Box className="pop-over-container">
          <List component="nav" aria-label="main mailbox folders">
            {/* User sign in */}
            {isUserLogIn && (
              <div>
                <ListItemButton
                  onClick={(event) => handleOnItemButtonClick("")}
                >
                  <ListItemIcon>
                    <PermIdentityOutlined />
                  </ListItemIcon>
                  <ListItemText primary="Edit Profile" />
                </ListItemButton>
                <ListItemButton
                  onClick={(event) => handleOnItemButtonClick("")}
                >
                  <ListItemIcon>
                    <GradeSharp />
                  </ListItemIcon>
                  <ListItemText primary="Favorite" />
                </ListItemButton>
                <Divider />
                <ListItemButton
                  onClick={(event) => setUserSingOut("/sign_out")}
                >
                  <ListItemIcon>
                    <LogoutIcon />
                  </ListItemIcon>
                  <ListItemText primary="Sign out" />
                </ListItemButton>
              </div>
            )}

            {/* User does not sign in */}
            {!isUserLogIn && (
              <ListItemButton
                onClick={(event) => {
                  handleOnSignInClick();
                }}
              >
                <ListItemIcon>
                  <LoginOutlined />
                </ListItemIcon>
                <ListItemText primary="Sign in" />
              </ListItemButton>
            )}
          </List>
        </Box>
      </Popover>
      <LoginDialog
        isOpenDialog={isOpenDialog}
        onCloseDialog={() => {
          setIsOpenDialog(false);
        }}
      />
    </div>
  );
};

HeaderBarAvatar.propTypes = {};

export default HeaderBarAvatar;
