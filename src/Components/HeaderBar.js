import {
  DensitySmallSharp,
  FeedSharp,
  MenuBookRounded,
} from "@mui/icons-material";
import { AppBar, Tab, Tabs, Toolbar } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { useNavigate } from "react-router-dom";
import "./HeaderBar.css";
import HeaderBarAvatar from "./HeaderBarAvatar";
import SearchBar from "./SearchBar";

export default function HeaderBar(props) {
  const navigate = useNavigate();
  const handleSearchBarSubmit = (searchValue) => {
    props.onSearchBarSubmit(searchValue);
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" id="header-bar-app-bar">
        <Toolbar>
          <Tabs value={0} textColor="inherit" variant="fullWidth">
            <Tab
              className="header-bar-logo-image"
              onClick={() => {
                navigate("/");
              }}
            />
            <Tab
              className="header-bar-tab"
              icon={<DensitySmallSharp />}
              label="A-Z List"
            />
            <Tab
              className="header-bar-tab"
              icon={<MenuBookRounded />}
              label="Book Mark"
            />
            <Tab className="header-bar-tab" icon={<FeedSharp />} label="News" />
          </Tabs>
          <SearchBar
            handleSearchDone={props.handleSearchDone}
            className="header-bar-search-bar-container"
            onSearchBarSubmit={handleSearchBarSubmit}
          ></SearchBar>
          <HeaderBarAvatar></HeaderBarAvatar>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
