import SearchIcon from "@mui/icons-material/Search";
import { Divider, InputBase, Paper } from "@mui/material";
import IconButton from "@mui/material/IconButton";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./SearchBar.css";

/**
 *  Test using sx css property;
 *  It's same like inline style
 *  But you can using calculate function
 * */
const cssProperty = {
  paper: {
    p: "2px 4px",
    display: "flex",
    alignItems: "center",
  },
  input: { ml: 1, flex: 1 },
  divider: { height: 28, m: 0.5 },
  icon: { p: "10px" },
};

const SearchBar = (props) => {
  const [searchValue, setSearchValue] = useState("");
  const [recentSearchList, setRecentSearchList] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    // Search value is not in recent search list
    if (
      searchValue &&
      searchValue.trim().length > 0 &&
      !recentSearchList.includes(searchValue.trim())
    ) {
      setRecentSearchList([...recentSearchList, searchValue]);
      setSearchValue("");
    }
  }, [props.handleSearchDone]);

  /**
   * Emit search value to parent
   * Set add new search value to recent search list
   * @param {HTML event listener} event
   */
  const handleSearchBarSubmit = (event) => {
    const submitSearchValue = event.target[0].value.trimEnd();
    props.onSearchBarSubmit(submitSearchValue);
    setSearchValue(submitSearchValue);
    navigate(`/search?title=${submitSearchValue}`);
    event.preventDefault();
  };

  const handleHrefClick = (e, uri) => {
    e.preventDefault();
    navigate(uri);
  };

  const renderedRecentSearchList = () =>
    recentSearchList.map((recentSearch, index) => {
      return (
        <a
          key={index}
          className="search-bar-recent-search"
          href="/"
          onClick={(e) => {
            handleHrefClick(e, `/search?title=${recentSearch}`);
          }}
        >
          {recentSearch}
        </a>
      );
    });

  return (
    <div className="search-bar-container">
      <div className="search-bar-field">
        <Paper
          component="form"
          sx={cssProperty.paper}
          onSubmit={handleSearchBarSubmit}
        >
          <InputBase
            sx={cssProperty.input}
            placeholder="Search Anime"
            onChange={(e) => setSearchValue(e.target.value)}
            value={searchValue}
          />
          <Divider sx={cssProperty.divider} orientation="vertical" />
          <IconButton type="submit" sx={cssProperty.icon} aria-label="search">
            <SearchIcon />
          </IconButton>
        </Paper>
      </div>
      <div className="search-bar-recent-search-box">
        {renderedRecentSearchList()}
      </div>
    </div>
  );
};

export default SearchBar;
