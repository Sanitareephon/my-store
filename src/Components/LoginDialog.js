import {
  Alert,
  Button,
  Modal,
  Paper,
  Tab,
  Tabs,
  TextField,
} from "@mui/material";
import { Box } from "@mui/system";
import e from "cors";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "../Components/LoginDialog.css";
import { addNewUser, setUserIdLS, signIn } from "../Utils/utils";

export function LoginDialog(props) {
  const { onCloseDialog, selectedValue, isOpenDialog } = props;
  const [userId, setUserId] = useState("");
  const [password, setPassword] = useState("");
  const [renderedErrorMessage, setRenderedErrorMessage] = useState(<div></div>);
  const [tabValue, setTabValue] = useState(0);
  const nvaigate = useNavigate();

  const handleTabChange = (event, newValue) => {
    setRenderedErrorMessage(<div></div>);
    setTabValue(newValue);
  };

  const handleCloseDialog = (isDi) => {
    setRenderedErrorMessage(<div></div>);
    setUserId("");
    setPassword("");
    setTabValue(0);
    onCloseDialog(true);
  };

  const handleSignInClick = () => {
    const isSignInSuccess = signIn(userId, password);
    if (isSignInSuccess) {
      setUserIdLS(userId);
      nvaigate("/");
      window.location.reload();
    } else {
      setRenderedErrorMessage(
        <div className="log-in-error-message">
          <Alert severity="error">Username or Password is incorrect.</Alert>
        </div>
      );
    }
  };

  const handleSignUpClick = () => {
    const isSignUpSuccess = addNewUser(userId, password);
    if (isSignUpSuccess) {
      setUserIdLS(userId);
      nvaigate("/");
      window.location.reload();
    } else {
      setRenderedErrorMessage(
        <div className="log-in-error-message">
          <Alert severity="error">Username already exist.</Alert>
        </div>
      );
    }
    handleCloseDialog(isSignUpSuccess);
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();
    tabValue === 0 ? handleSignInClick() : handleSignUpClick();
  };

  return (
    <div>
      <Modal
        open={isOpenDialog}
        onClose={handleCloseDialog}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        className="login-dialog-container"
      >
        <Paper className="log-in-dialog-layout">
          <form className="log-in-dialog-content">
            <Box className="log-in-dialog-box">
              <div className="log-in-logo"></div>
              <Tabs
                value={tabValue}
                onChange={handleTabChange}
                aria-label="basic tabs example"
                centered
              >
                <Tab label="SIGN IN" className="log-in-tab" />
                <Tab label="SIGN UP" className="log-in-tab" />
              </Tabs>

              <div className="log-in-form">
                <div className="log-in-text-field-box">
                  <TextField
                    label="Username"
                    value={userId}
                    className="log-in-text-field"
                    variant="outlined"
                    onChange={(e) => setUserId(e.target.value)}
                  />
                </div>
                <div className="log-in-text-field-box">
                  <TextField
                    label="Password"
                    type="password"
                    className="log-in-text-field"
                    value={password}
                    variant="outlined"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>
              </div>
              <div>
                <Button
                  className="log-in-form-button"
                  variant="contained"
                  type="submit"
                  onClick={handleOnSubmit}
                >
                  {tabValue === 0 ? "SIGN IN" : "SIGN UP"}
                </Button>
                {renderedErrorMessage}
              </div>
            </Box>
          </form>
        </Paper>
      </Modal>
    </div>
  );
}
export default LoginDialog;
