import React, { Component } from "react";
import "./LinkButton.css";
export default class LinkButton extends Component {
  render() {
    return (
      <a className="link-button-container" href="/">
        <div className="link-button-picture"></div>
        <div className="link-button-message">Category Name</div>
      </a>
    );
  }
}
