import {
  FavoriteBorderOutlined,
  FavoriteRounded,
  Share,
} from "@mui/icons-material";
import {
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  IconButton,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  getCoverImageApi,
  getImageHostLocation,
} from "../../Services/SearchMangaService";
import {
  getRelationsCoverId,
  getUserFavoritesLS,
  getUserIdLS,
  setUserFavoritesLS,
} from "../../Utils/utils";
import LoginDialog from "../LoginDialog";
import "./ProductCard.css";

// Test using custom style
const useStyles = makeStyles((theme) => ({
  customHoverFocus: {
    "&:hover, &.Mui-focusVisible": {
      backgroundColor: "#EBFAFC",
    },
  },
}));

export default function ProductCard(props) {
  const [productImgPath, setProductImgPath] = useState("");
  const { attributes, relationships, type, id } = props.cardInfo;
  const [isOpenDialog, setIsOpenDialog] = useState(false);
  const [isFavorite, setIsFavorite] = useState(false);
  const [mangaTitle, setMangaTitle] = useState("");
  const customStyle = useStyles();
  const navigate = useNavigate();

  useEffect(() => {
    setProductImgPath("#");
    const userFavoirteLS = getUserFavoritesLS();
    const isUserFavorite = !!userFavoirteLS
      ? userFavoirteLS.includes(id)
      : false;
    setIsFavorite(isUserFavorite);
    (async () => {
      // Distract cover id from manga info
      const coverId = getRelationsCoverId(relationships);
      if (coverId) {
        // Get image path from manga cover api
        const coverImageResponse = await getCoverImageApi(coverId);
        const coverAttributes = coverImageResponse.data.data.attributes;
        // Set image path
        const imagePath = getImageHostLocation(id, coverAttributes.fileName);
        setProductImgPath(imagePath);
        setMangaTitle(getEnMangaTitle());
      }
    })();
  }, [props.cardInfo]);

  // Filter only manga title with en language
  const getEnMangaTitle = () => {
    let title = "";
    if (attributes) {
      const filterList = attributes.altTitles.filter((title) => !!title.en);
      title = filterList.length > 0 ? filterList[0].en : attributes.title.en;
    }
    return title;
  };

  const mangaDescription = (
    <div>
      <div title={mangaTitle} className="product-card-title">
        {mangaTitle}{" "}
      </div>
      <div className="product-card-chapter">
        <span>{`Last Chapter : ${attributes.lastChapter}`}</span>
      </div>
    </div>
  );

  const handleFavoriteIconClick = () => {
    const userId = getUserIdLS();
    if (userId) {
      setIsFavorite(!isFavorite);
      setUserFavoritesLS(id);
    } else {
      setIsOpenDialog(true);
    }
  };

  const handleProductItemClick = (e) => {
    navigate(`/product/${id}`);
  };

  let favoriteElement = <FavoriteBorderOutlined />;
  if (isFavorite) {
    favoriteElement = <FavoriteRounded style={{ color: "#e91e63" }} />;
  }

  return (
    productImgPath !== "#" && (
      <Card className="product-card-container">
        <CardActionArea className="product-card-action-area">
          <CardMedia
            component="img"
            className="product-card-image"
            src={productImgPath}
            alt={attributes.title.en}
            onClick={handleProductItemClick}
          ></CardMedia>
          <div className="overlay-image"></div>
        </CardActionArea>
        <CardContent className="product-card-content">
          <div
            variant="body2"
            color="text.secondary"
            className="product-card-description"
          >
            {mangaDescription}
          </div>
        </CardContent>
        <CardActions className="product-card-actions">
          <IconButton
            color="primary"
            aria-label="upload picture"
            component="span"
            className={`${customStyle.customHoverFocus}`}
          >
            <Share />
          </IconButton>
          <IconButton
            size="small"
            color="primary"
            className={`${customStyle.customHoverFocus} product-card-favorite-icon`}
            onClick={handleFavoriteIconClick}
          >
            {favoriteElement}
          </IconButton>
        </CardActions>

        <LoginDialog
          isOpenDialog={isOpenDialog}
          onCloseDialog={(isCloseDialog) => {
            setIsOpenDialog(!isCloseDialog);
          }}
        />
      </Card>
    )
  );
}
