import { Link, Pagination } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
import "./PagePagination.css";
const PagePagination = (props) => {
  const navigate = useNavigate();
  const lastPage = !!props.lastPage ? props.lastPage : 0;

  const onPaginationClick = (event, pageNumber) => {
    event.preventDefault();
    navigate(`/page/${pageNumber}`);
    props.onPaginationChange(pageNumber);
  };

  return (
    <div className="page-pagination-container">
      <Pagination
        size="large"
        count={lastPage}
        page={props.currentPageNumber}
        onChange={onPaginationClick}
      />
      <Link to="page/1">About</Link>
    </div>
  );
};

export default PagePagination;
