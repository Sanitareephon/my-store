// Distract cover id from manga info
const getRelationsCoverId = (relationships) => {
  let coverArtsId = null;
  const coverArts = relationships.filter(
    (relationship) => relationship.type === "cover_art"
  )[0];
  coverArtsId = coverArts ? coverArts.id : null;
  return coverArtsId;
};

// Get manga EN chapter
const getEnChapterList = (mangaFeed) => {
  const enChapterList = mangaFeed.filter(
    (feed) =>
      feed.attributes.translatedLanguage === "en" && feed.attributes.pages > 0
  );
  return enChapterList;
};

const getUserIdLS = () => {
  return window.localStorage.getItem("user_id");
};

const userSignOutLS = () => {
  window.localStorage.removeItem("user_id");
  window.location.reload();
};

const setUserIdLS = (userId) => {
  if (!!userId) {
    window.localStorage.setItem("user_id", userId);
    const isExistingUser = window.localStorage.getItem(userId);
    // Create new user is user not existing
    if (!isExistingUser) {
      setLocalStorage(userId, {});
    }
  }
};

const getCurrentUserIdLS = () => {
  return window.localStorage.getItem("user_id");
};

/**
 * Toggle favorite manga id
 * e.g. like -> unlike / unlike -> like
 * @param {manga id} mangaId
 */
const setUserFavoritesLS = (mangaId) => {
  const userId = getCurrentUserIdLS();
  const currentUserSetting = getLocalStorage(userId);
  // Favorite list not exist on this user
  if (!currentUserSetting.favoriteList) {
    currentUserSetting.favoriteList = [mangaId];
  } else if (currentUserSetting.favoriteList.includes(mangaId)) {
    // User remove manga from favorite list
    currentUserSetting.favoriteList = currentUserSetting.favoriteList.filter(
      (favorite) => favorite !== mangaId
    );
  } else if (!currentUserSetting.favoriteList.includes(mangaId)) {
    // User add manga to favorite list
    currentUserSetting.favoriteList = [
      ...currentUserSetting.favoriteList,
      mangaId,
    ];
  }
  setLocalStorage(userId, currentUserSetting);
};

const getLocalStorage = (localStorageName) => {
  const lsItem = window.localStorage.getItem(localStorageName);
  return lsItem !== undefined ? JSON.parse(lsItem) : null;
};
const setLocalStorage = (localStorageName, value) => {
  window.localStorage.setItem(localStorageName, JSON.stringify(value));
};

const getUserListLocalStorage = () => {
  const userList = getLocalStorage("user_list");
  return userList ? userList : [];
};

const signIn = (userName, password) => {
  const userList = getUserListLocalStorage();
  const filterUser = userList.filter(
    (userpass) => userpass.userId === userName && userpass.password === password
  );
  return filterUser ? filterUser.length > 0 : false;
};

const addNewUser = (userName, password) => {
  const userList = getUserListLocalStorage();
  const filterUser = userList.filter(
    (userpass) => userpass.userId === userName
  );
  const isUserExist = filterUser ? filterUser.length > 0 : false;
  if (isUserExist) {
    return false;
  } else {
    const newUserList = [...userList, { userId: userName, password }];
    setLocalStorage("user_list", newUserList);
    return true;
  }
};

/**
 * Get list of user's favorite manga
 * @param {user id} userName
 * @returns List of user's favorite manga
 */
const getUserFavoritesLS = () => {
  const userId = getUserIdLS();
  const userSetting = getLocalStorage(userId);
  return userSetting ? userSetting.favoriteList : [];
};

export {
  getUserIdLS,
  setUserIdLS,
  setUserFavoritesLS,
  getUserFavoritesLS,
  getRelationsCoverId,
  getEnChapterList,
  userSignOutLS,
  getUserListLocalStorage,
  signIn,
  addNewUser,
};
