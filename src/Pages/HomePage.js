import { Breadcrumbs, Container, Link, Stack } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import ProductCard from "../Components/Common/ProductCard";
import { getInitialLoadMangaApi } from "../Services/SearchMangaService";
import { userSignOutLS } from "../Utils/utils";
import "./HomePage.css";

export default function HomePage(props) {
  const { pageNumber } = useParams();
  const { onSearchDone } = props;
  const search = useLocation().search;
  const navigate = useNavigate();
  const location = useLocation();
  const queryParameterTitle = new URLSearchParams(search).get("title");
  const searchParams = queryParameterTitle ? queryParameterTitle : "";
  const [homePageState, setHomePageState] = useState({
    mangaList: [],
    pageNumber: !!pageNumber ? +pageNumber : 1,
    lastPage: 1,
    renderedManga: <div></div>,
  });

  if (location.pathname === "sign_out") {
    userSignOutLS();
  }
  // Handle initial load
  useEffect(() => {
    (async () => {
      const initialLoadMangaResponse = await getInitialLoadMangaApi(
        searchParams
      );
      const initialLoadMangaData = initialLoadMangaResponse.data.data;
      setHomePageState({
        ...homePageState,
        mangaList: initialLoadMangaData,
        renderedManga: [...generateRenderedManga(initialLoadMangaData)],
      });
      onSearchDone();
    })();
    return () => {};
  }, [queryParameterTitle]);

  // Rendered manga list
  const generateRenderedManga = (mangaList) => {
    let newRenderedManga = <div></div>;
    if (!!mangaList) {
      newRenderedManga = [...mangaList].map((data, index) => (
        <ProductCard key={index} cardInfo={data}></ProductCard>
      ));
    }
    return newRenderedManga;
  };

  const handleHrefClick = (e, uri) => {
    e.preventDefault();
    navigate(uri);
  };

  return (
    <Container>
      <div className="home-page-container">
        {!!searchParams && (
          <Stack spacing={2} className="home-page-bread-crumbs">
            <Breadcrumbs separator="›" aria-label="breadcrumb">
              {[
                <Link
                  underline="hover"
                  key="1"
                  color="inherit"
                  onClick={(e) => {
                    handleHrefClick(e, "/");
                  }}
                >
                  Home
                </Link>,
                <Link
                  underline="hover"
                  key="2"
                  color="inherit"
                  href="/"
                  onClick={(e) => {
                    handleHrefClick(e, `/search?title=${searchParams}`);
                  }}
                >
                  Quick Search:
                </Link>,
              ]}
              <span>{searchParams}</span>
            </Breadcrumbs>
          </Stack>
        )}
        {/* <div className="home-page-recommend-products"></div>
      <div className="home-page-category-menu">
        <CategoryMenu></CategoryMenu>
      </div> */}
        <div className="home-page-products">{homePageState.renderedManga}</div>
        {/* <div className="home-page-pagination">{renderedPagination}</div> */}
        {/* <div className="home-page-footer">
        <PageFooter></PageFooter>
      </div> */}
      </div>
    </Container>
  );
}
