import { CardMedia } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import {
  getCoverImageApi,
  getImageHostLocation,
  getMangaFeedApi,
  getMangaInfoByIdApi,
} from "../Services/SearchMangaService";
import { getEnChapterList, getRelationsCoverId } from "../Utils/utils";
import "./ProductDetailPage.css";

const ProductDetailPage = (props) => {
  const { productId } = useParams();
  const [mangaInfo, setMangaInfo] = useState(null);
  const [productImgPath, setProductImgPath] = useState("");
  const [mangaFeed, setMangaFeed] = useState(null);
  useEffect(() => {
    (async () => {
      // Get manga info by id
      const mangaInfoResponse = await getMangaInfoByIdApi(productId);
      const mangaInfoData = mangaInfoResponse.data.data;
      setMangaInfo(mangaInfoData);
      // Get Cover art
      const mangaCoverId = getRelationsCoverId(mangaInfoData.relationships);
      const coverImageResponse = await getCoverImageApi(mangaCoverId);
      const coverAttributes = coverImageResponse.data.data.attributes;
      const imagePath = getImageHostLocation(
        mangaInfoData.id,
        coverAttributes.fileName
      );
      setProductImgPath(imagePath);
      // Get manga feed
      const mangaFeedResponse = await getMangaFeedApi(mangaInfoData.id);
      setMangaFeed(mangaFeedResponse.data.data);
    })();
  }, []);

  const renderedProperties = (propertiesName, propertiesValue) => {
    return !!propertiesValue ? (
      <div className="display-value">
        {propertiesName}
        {<p>{JSON.stringify(propertiesValue)}</p>}
      </div>
    ) : (
      <div>{propertiesName}</div>
    );
  };

  const renderedChapterList = () => {
    return !!mangaFeed ? (
      getEnChapterList(mangaFeed).map((feed, index) => {
        return (
          <div key={index}>
            <span>
              Chapter: {feed.attributes.chapter} {feed.attributes.title}
            </span>
          </div>
        );
      })
    ) : (
      <div>Not found chapter</div>
    );
  };

  return (
    <div className="product-detail-page-container">
      <div>Product Detail Page</div>
      <div>
        <div>
          <CardMedia
            component="img"
            className="product-detail-card-image"
            src={productImgPath}
            // alt={mangaInfo.attributes.title.en}
            sizes=""
          ></CardMedia>
        </div>
        <div>{renderedProperties("Manga Info", mangaInfo)}</div>
        <div>{renderedProperties("Manga feed", mangaFeed)}</div>
        <div className="chapter-container">{renderedChapterList()}</div>
      </div>
    </div>
  );
};

export default ProductDetailPage;
