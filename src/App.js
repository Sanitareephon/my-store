import { ThemeProvider } from "@emotion/react";
import { createTheme } from "@mui/material";
import { useEffect, useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import "./App.css";
import HeaderBar from "./Components/HeaderBar";
import HomePage from "./Pages/HomePage";
import ProductDetailPage from "./Pages/ProductDetailPage";
import { setUserIdLS } from "./Utils/utils";

const theme = createTheme();

function App() {
  const [searchValue, setSearchValue] = useState("");
  const [toggleRefreshComponent, setToggleRefreshComponent] = useState(false);
  const homePageRouteURI = [
    "/",
    "/page:pageId",
    "/search",
    "sign_out",
    "sign_in",
  ];

  useEffect(() => {
    setUserIdLS();
    // Add event listener to scrollbar for showing/hiding header bar
    let prevScrollpos = window.pageYOffset;
    document.getElementById("content-layout").onscroll = function () {
      var currentScrollPos =
        document.getElementById("content-layout").scrollTop;
      if (prevScrollpos > currentScrollPos) {
        document.getElementById("header-bar-app-bar").style.marginTop = "0";
      } else {
        document.getElementById("header-bar-app-bar").style.marginTop = "-90px";
      }
      prevScrollpos = currentScrollPos;
    };
  }, []);

  const handleSearchBarSubmit = (searchMangaResults) => {
    setSearchValue(searchMangaResults);
  };

  const handleSearchDone = () => {};

  const renderedHomePageRoute = () =>
    homePageRouteURI.map((uri, index) => {
      return (
        <Route
          key={index}
          path={uri}
          element={
            <HomePage
              onSearchDone={handleSearchDone}
              searchValue={searchValue}
            ></HomePage>
          }
        ></Route>
      );
    });

  // const handlePaginationClick = (pagination) => {};
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <HeaderBar
            handleSearchDone={handleSearchDone}
            onSearchBarSubmit={handleSearchBarSubmit}
            refreshComponent={toggleRefreshComponent}
          ></HeaderBar>
          <div id="content-layout">
            <Routes>
              {renderedHomePageRoute()}
              <Route
                path="/product/:productId"
                element={
                  <ProductDetailPage
                    searchValue={searchValue}
                  ></ProductDetailPage>
                }
              ></Route>
            </Routes>
          </div>
        </BrowserRouter>
      </ThemeProvider>
    </div>
  );
}

export default App;
