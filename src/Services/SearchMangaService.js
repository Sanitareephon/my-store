import axios from "axios";
const hostName = "https://api.mangadex.org";
const imageHostName = "https://uploads.mangadex.org";

// Query parameter for manga API
const orderTitleAsc = "order[title]=asc";
const havingChapter = "hasAvailableChapters=true";
const limitedFifty = "limit=50";
const chapterOrderAsc = "order[chapter]=asc";
// Api
const mangaApi = `${hostName}/manga`;
const mangaFeedApi = (mangaId) => `${hostName}/manga/${mangaId}/feed`;

// Top search service
const getInitialLoadMangaApi = (searchValue = "") => {
  const titleName = searchValue.length > 0 ? `title=${searchValue}` : "";
  const queryParamList = [titleName, limitedFifty, orderTitleAsc];
  return axiosRequest(mangaApi, queryParamList);
};

const getMangaInfoByIdApi = (mangaId) => {
  return axios.request(`${hostName}/manga/${mangaId}`);
};

const getCoverImageApi = (mangaId) => {
  const coverImageApi = `${hostName}/cover/${mangaId}`;
  return axios.get(coverImageApi);
};
const getMangaFeedApi = (mangaId) => {
  const queryParamList = [chapterOrderAsc];
  return axiosRequest(mangaFeedApi(mangaId), queryParamList);
};

// Utilities function
/**
 * Combine api request and query parameter
 * @param {Api name} api
 * @param {List of query parameter} param1
 * @returns
 */
const axiosRequest = (api, [...queryParamList]) => {
  let requestApiWithQueryParam = api;
  queryParamList.forEach((query) => {
    const isQueryAvailable = !!query && query.length > 0;
    if (isQueryAvailable) {
      const querySign = !requestApiWithQueryParam.includes("?") ? "?" : "&";
      requestApiWithQueryParam += querySign + query;
    }
  });
  return axios.request(requestApiWithQueryParam);
};

const getImageHostLocation = (mangaId, fileName) =>
  `${imageHostName}/covers/${mangaId}/${fileName}`;

export {
  // Api
  getInitialLoadMangaApi,
  getMangaInfoByIdApi,
  getCoverImageApi,
  getMangaFeedApi,
  // Utilities function
  getImageHostLocation,
};
